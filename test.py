import pytest
from app import material

@pytest.mark.parametrize (
    'spaceship, result',
    [
        ([6, 2, 1, 1, 8, 0, 5, 5, 0, 1, 8, 9, 6, 9, 4, 8, 0, 0], 50),
        ([6, 4, 2, 0, 3, 2, 0, 3, 1, 4, 5, 3, 2, 7, 5, 3, 0, 1, 2, 1, 3, 4, 6, 8, 1, 3], 83),
        ([0,1,0,2,1,0,1,3,2,1,2,1], 6),
        ([0,1,0,2,1,0,3,1,0,1,2], 8),
        ([4,2,0,3,2,5], 9)
    ]
)
def test_given_examples(spaceship, result):
    assert material(spaceship) == result

@pytest.mark.parametrize (
    'spaceship, result',
    [
        ([], 0),
        ([0, 0, 0, 1, 1, 1, 1], 0),
        ([2], 0),
        ([1,0,1], 1),
    ]
)
def test_edge_cases(spaceship, result):
    assert material(spaceship) == result

