def material(spaceship: list[int]) -> int:
    length = len(spaceship)
    
    if length < 3:
        return 0

    start = 0
    end = length-1

    for i in range(length):
        if spaceship[i] != 0:
            break
        start = i

    for i in reversed(range(length)):
        if spaceship[i] != 0:
            break
        end = i

    max = spaceship[start]
    max_index = start
    current_biggest = 0
    current_biggest_index = 0

    result_table = []

    for i in range(start+1, end+1):
        value = spaceship[i]
        if value >= max:
            result_table.append((max, max_index, max, i))
            max = value
            max_index = i
            current_biggest = 0
            current_biggest_index = 0
        else:
            if value >= current_biggest:
                current_biggest = value
                current_biggest_index = i

    if (max_index != end):
        result_table.append((max, max_index, current_biggest, current_biggest_index))

    result = 0

    for x in result_table:
        level = x[0] if x[0] < x[2] else x[2]
        result += level*(x[3] - x[1] - 1) - sum(spaceship[x[1]+1:x[3]])

    return result
