# Code Wars

## Open for job opportunities :)

- https://www.linkedin.com/in/rafa%C5%82-mielniczuk-120173225/
- rmielniczuk001@gmail.com
- 266508@student.pwr.edu.pl

## Perks

- Cambridge English: Advanced (CAE)
- Intermediate German - C1 course done, certificate to be done
- 5th semester of Applied Computer Science at Wroclaw University of Science and Technology
- Linux as daily driver
- Attending student research group CoffeeBreak-Wroclaw
- Intermediate proficiency in Android programming
- Fixing old MacBooks Pro in free time
- Known programming languages:
    - C
    - C++
    - __Python__ (biggest experience so far)
    - Kotlin
